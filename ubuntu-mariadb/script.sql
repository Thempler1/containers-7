CREATE DATABASE mydb;
USE mydb;
CREATE TABLE mitabla ( id INT PRIMARY KEY, nombre VARCHAR(20) );
INSERT INTO mitabla VALUES ( 1, 'Will' );
INSERT INTO mitabla VALUES ( 2, 'Marry' );
INSERT INTO mitabla VALUES ( 3, 'Dean' );