--Ubuntu-mariaDB
Sel contenedor actual tiene como propósito ejecutar un contenedor con una imagen de Ubuntu
en la cual se instala MariaDB-server para crear una base de datos e ingresar datos mediante el script "script.sql"

NOTA:
si al ejecutar eel comando:
 "RUN mysqld_safe --skip-grant-tables --skip-networking"
el sistema queda cargando, comentar las lineas 21,22,23 del docker file y ejecutar manualmente
si el problema con el comando antes mencionado persiste al ejecutar manualmente, intentar con ctrl+c volver a intentarlo, luego seguir con las lineas 22 y 23.

El problema es causado ya que al instalar mariaDB no se conmfigura una contraseña para ingresar, por lo cual debemos ingresar utilizando ese bloque de comandos (21,22) para poder ingresar sin pedir la contraseña. La linea 23 ejecuta el script.sql que contiene las instrucciones para la creacion de la base de datos, sus tablas e ingreso de datos.